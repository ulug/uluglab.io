---
title: "Årsmötesprotokoll 2011"
date: 2011-06-10
---

Bifogat finns protokollet och revisionsberättelse (samt signaturfiler) från årsmötet 2011-05-28.

[Protokoll](/protokoll/2011/2011-05-28 Årsmötesprotokoll.pdf)  
[Signatur Anders](/protokoll/2011/2011-05-28 Årsmötesprotokoll.pdf.sig-anders)  
[Signatur Erik](/protokoll/2011/2011-05-28 Årsmötesprotokoll.pdf.sig-erik)  
[Signatur Gabriel](/protokoll/2011/2011-05-28 Årsmötesprotokoll.pdf.sig-gabriel)  
[Signatur Martin](/protokoll/2011/2011-05-28 Årsmötesprotokoll.pdf.sig-martin)  
[Rev_ULUG-2010.pdf](/protokoll/2011/Rev_ULUG-2010.pdf)  
[Rev_ULUG-2010.pdf.gpg](/protokoll/2011/Rev_ULUG-2010.pdf.gpg)  
