---
title: "Hackträff - online - lördag den 29 januari!"
date: 2021-12-15
draft: false
---

Vi hade planerat en hackträff till lördag den 29, men på grund av smittoläget har vi beslutat att hålla det online istället.

- Tid: 16 - 18
- Videoplattform: Jitsi
- Länk: [meet.friprogramvarusyndikatet.se/ulug2022](https://meet.friprogramvarusyndikatet.se/ulug2022)
