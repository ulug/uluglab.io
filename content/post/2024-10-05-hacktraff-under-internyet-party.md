---
title: "Hackträff på Internyet Party"
date: 2024-09-30
draft: false
---

**Tid:** lördag den 5 oktober klockan 10 till ca 16.

**Plats:** [Nya Brantingsskolans aula](https://osm.org/go/0bGGEY58e?layers=N&m=), vid Brantingstorget

I helgen ansluter Ulug till [The Internyet Party](https://internyet.party/invite/) som är ett internetfrånkopplat LAN-party. Där håller vi lördagens hackträff.

[The Internyet Party](https://internyet.party/invite/) kan beskrivas som "imagine that the Chaos Communication Congress and Birdie à la 2009 had a love child during a post-apocalyptic network outage."


