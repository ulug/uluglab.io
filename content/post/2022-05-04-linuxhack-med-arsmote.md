---
title: "Linuxhack med årsmöte"
date: 2022-05-06
draft: false
---

**Tid:** lördag den 21 maj klockan 10 till ca 17.

**Plats:** [Skolgatan 6 coworking space](https://www.openstreetmap.org/node/9747185809)

Årsmötet är planerat till klockan 14. Du är välkommen att vara med på Linuxhacket även om du inte avser att delta i årsmötet. [Se kallelsen till årsmötet här!](https://www.ulug.se/post/2022-04-06--kallelse-till-arsmote/).

I övrigt gäller det vanliga - hacka på ditt projekt, hårdvara som mjukvara. Installera en exotisk Linuxvariant, eller bjud med någon som aldrig provat fri mjukvara - Valet är ditt! Detta är ett tillfälle att diskutera teknik och lösningar med den stora samlade kunskapsbasen som finns i ULUG, och ett tillfälle att utbyta erfarenheter med andra medlemmar.

Oavsett om du vill sitta och hacka i gott sällskap, eller jobba i något gemensamt projekt är du varmt välkommen till ULUG mini-hackathon. Vill du bara dricka kaffe och prata med folk utan att ha något direkt projekt går det lika bra!

