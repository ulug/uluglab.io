---
title: "Workshop mo 9 may: What is a command line? Beginners welcome"
date: 2022-04-26
draft: false
---
One commonly interacts with computers through a graphical user interface (GUI), for example the desktop interface of Microsoft Windows or Mac OS. However, you can also use a text interface known as the command line (or the terminal, shell, console or prompt). While it may appear complex at first sight, it offers a lot of power and flexibility for telling the computer what to do. Some programs and installations require usage of the command line.

![Command line](https://secure.meetupstatic.com/photos/event/3/a/e/a/highres_503655082.jpeg "a command line icon")

This workshop will introduce you to the command line and walk you through a variety of fundamental concepts and commands. No prior knowledge is assumed. Bring your own computer with the possibility to insert a USB A cable.

The workshop is at Engelska Parken, you do not need any student card to enter. Go to the room 4-0027 and ring the bell, I will open you.

**Mon, May 9, 18.30**

Address:
Engelska parken
Thunbergsvägen 3C
Uppsala

See on the map of the campus here:
https://link.mazemap.com/HM8rBhDP

Or look at this video to see how to enter from humanistiska teatern and where to ring the bell:
https://diode.zone/w/5yeKYBr7pTUhRuG7kSALXr
