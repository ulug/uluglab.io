---
title: "Ny hemsida"
date: 2009-07-29
---

Nu är äntligen vår nya hemsida online! Vi har valt att börja utveckla en egen mjukvara för att hantera hemsidan, detta under namnet FöreningsCMS. Alla - medlemmar som icke medlemmar - är välkommna att delta i utvecklingsarbetet!