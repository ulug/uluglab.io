---
title: "Våraktivitet"
date: 2011-03-06
---

Nu när den långa mörka kalla vintern så smått börjar lida mot sitt slut är det dags att dra igång aktiviteterna i föreningen igen. Vi träffas nu på tisdag för ett traditionsenligt fika. Se kalendern för mer information.