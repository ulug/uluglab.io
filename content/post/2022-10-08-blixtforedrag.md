---
title: "Hackträff med blixtföredrag - lör 8 okt"
date: 2022-09-20
draft: false
---

Välkomna till en föreläsningsdag med Uppsala Linux User Group! Vi har alla något intressant som vi kan dela med oss av - nåt som vi jobbar med, nåt som vi har som hobby, någon fri programvara som vi vill rekommendera, nåt coolt som man har byggt eller nåt ämne som vi brinner för.

Därför bjuder vi in till en dag med korta föredrag. Vi tänker oss både femminuters blixtföredrag och längre upp till en halvtimme. Hittills har vi följande punkter inplanerade, men vi uppdaterar listan allt eftersom:

- Vad är Fediverse?
- Docker och Kubernetes
- Smalltalk - ett litet språk med en stor historia
- Mjukvarudefinierad radio
- Hur fungerar en kortbetalning?
- Northvolts IOT-plattform
- Säkerhets- och licensskanning av Open Source
- Skräddarsy en slimmad distribution med Yocto

Vill du hålla ett föredrag? Det vore välkommet. Hör av dig till oss på [kontakt@ulug.se](kontakt@ulug.se)!

- Plats: [Skolgatan 6 Coworking space](https://www.openstreetmap.org/node/9747185809)
- Tid: Vi öppnar dörrarna klockan 10 och håller öppet till klockan 17. 

![King Penguins at Salisbury Plain" by Liam Quinn is licensed under CC BY-SA 3.0. To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/2.0/?ref=openverse.](../../img/penguins.jpg "King Penguins at Salisbury Plain")
