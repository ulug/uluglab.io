---
title: "Linuxfika onsdag 08 februari"
date: 2023-02-08
draft: false
---
Onsdag den 8 februari är den andra onsdagen i månaden och som vanligt är det då Linux-fika!

Så välkommen till Café Linné, [Svartbäcksgatan 24](https://www.openstreetmap.org/node/459265410). Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux").
