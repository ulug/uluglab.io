---
title: "Hackträff 20 augusti"
date: 2022-08-14
draft: false
---
Nu är det äntligen dags för hackträff igen och det blir på samma ställe som förra gången, nämligen på co-working spacet på [Skolgatan 6](https://www.openstreetmap.org/node/9747185809)

Våra hackträffar brukar gå till så att folk kommer och går som de vill, var och en jobbar med sitt. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Ibland blir det mer eller mindre spontana korta föreläsningar eller demos i något intressant ämne. Utöver det finns det tid för snack, tips och inspiration och goda möten. Mitt på dagen brukar många gå ut och luncha tillsammans.

Vi öppnar dörrarna klockan 10 och håller öppet till klockan 17. 
