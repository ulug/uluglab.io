---
title: "Hackträff 11 februari`"
date: 2023-01-24
draft: false
---
Nu är det äntligen dags för hackträff igen och det blir på samma ställe som förra gången, nämligen på co-working spacet på [Skolgatan 6](https://www.openstreetmap.org/node/9747185809)

Vi har inget särskilt tema denna gång, men det finns utrymme för att hålla föredrag i något ämne vilket brukar vara uppskattat. Om du har problem med din dator, så är det ett utmärkt tillfälle att få hjälp. 

I övrigt är du välkommen att komma och hänga med oss, snacka teknik eller filosofi, jobba på dina eventuella projekt, inspireras av andra eller lära dig mer om fri programvara. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Kaffe finns, och mitt på dagen brukar många gå ut och luncha tillsammans.

Vi öppnar dörrarna klockan 10 och håller öppet till klockan 17. 
