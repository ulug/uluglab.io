---
title: "International repair day - med Uppsala Makerspace, lör 16 okt kl. 13"
date: 2021-10-09
draft: false
---

Vi firar [International Repair Day](https://openrepair.org/international-repair-day/) tillsammans med Uppsala Makerspace. Kanske har du något du tänkt reparera men som inte blivit av. Eller nåt som du vill laga men inte har kunskapen eller rätt verktyg. Häng med oss till [Uppsala Makerspace](https://uppsalamakerspace.se) på lördag, så hjälps vi åt med att laga saker. Där finns verkstäder för elektronik, trä, textil och metall - samt kunniga och hjälpsamma människor. Vill ha hjälp med att "reparera" din Linux-installation, så går det naturligtvis också bra!

- Plats: [Uppsala Makerspace, Ekebybruk 6M](https://www.openstreetmap.org/#map=19/59.84923/17.60795)
- Tid: lör 16 okt, 13 - 16

![Repair Day's logotyp](https://openrepair.org/wp-content/uploads/2021/05/RepairDay-2021-hero@2x-750x375.png, "Repair Day 2021-10-16")
