---
title: "Nya maillistor"
date: 2020-09-18
draft: false
---

Vår mailserver har funkat dåligt ett långt tag men nu har vi bytt server och hoppas på återupplivad aktivitet. Följande adresser är det som gäller nu:

* [utskick(snabel-a)ulug.se](mailto:utskick-request(snabel-a)ulug.se?subject=subscribe): Vår utskickslista, med information om kommande träffar, årsmöteskallelser med mera. Vi rekommenderar alla medlemmar att signa upp sig på den. Vi lovar att inte spamma er. Signa upp genom att skicka ett mail till [utskick-request(snabel-a)ulug.se?subject=subscribe](mailto:utskick-request(snabel-a)ulug.se?subject=subscribe).

* [diskussion(snabel-a)ulug.se](mailto:diskussion-request(snabel-a)ulug.se?subject=subscribe): Vår diskussionslista. Här kan du gå med om du vill diskutera med andra ulug-medlemmar, om ULUG:s verksamhet eller Linux- eller FOSS-relaterat i allmänhet. Signa upp genom att skicka ett mail till [mailto:diskussion-request(snabel-a)ulug.se?subject=subscribe](mailto:diskussion-request(snabel-a)ulug.se?subject=subscribe).

* <kontakt(snabel-a)ulug.se>: Vår standard-inbox, dit du kan maila om du vill ha kontakt med styrelsen.

* <ordforande(snabel-a)ulug.s>e: Om du vill skicka mail direkt till ordförande, kan du maila hit.

Vi gör alltså en nystart med maillistorna och hoppas att det ska passa alla. Har du kommentarer eller förslag, så gå gärna med i diskussionslistan, alternativt [maila styrelsen](mailto:kontakt(snabel-a)ulug.se).
