---
title: "Kallelse till årsmöte"
date: 2010-05-08
draft: false
---

Lördag den 5:e Juni är det dags för årsmöte. Dagordningen finns bifogad.

Motioner skickas till styrelse(snabel-a)ulug.org.

### Dagordning - Årsmöte 2010.
    Datum och tid: 2010-06-05 kl. 11:30
    Plats: Grillplatsen vid Sten Sture-monumentet

#### §1. Mötets öppnande.

#### §2. Fråga om mötets behöriga utlysande.

#### §3. Val av mötesfunktionärer

* Mötesordförande
* Mötessekreterare
* Två rösträknare och justerare

#### §4. Fastställande av röstlängd.

#### §5. Fastställande av dagordning.

#### §6.1. Verksamhetsberättelse.

#### §6.2. Ekonomisk berättelse.

#### §6.3. Revisionsberättelse.

#### §7. Frågan om ansvarsfrihet.

#### §8. Verksamhetsplan.

#### §9. Motioner & Propositioner.

#### §10. Medlemsavgift.

#### §11. Val av styrelse för det kommande verksamhetsåret.

* Ordförande*
* Kassör*
* Sekreterare*
* Vice ordförande
* Ledamöter
    (3-8 medlemmar totalt)

#### §12. Val av valberedare.

#### §13. Val av revisorer.

#### §14. Fastställande av teckningsrätt för det kommande året.

#### §15. Övriga frågor.

#### §16. Mötets avslutande.