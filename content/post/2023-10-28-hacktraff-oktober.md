---
title: "Hackträff 28 oktober"
date: 2023-10-22
draft: false
---

Det blir en hackträff **lördag den 28 oktober**. Vi har inte bestämt något särskilt tema denna gång, men förslag emottages gärna. Det finns också utrymme för att hålla föredrag i något ämne vilket brukar vara uppskattat.

Är du nybörjare eller bara nyfiken på fri programvara, så är du särskilt välkommen. Om du har problem med din dator, så är det ett utmärkt tillfälle att få hjälp.

I övrigt är du välkommen att komma och hänga med oss, snacka teknik eller filosofi, jobba på dina eventuella projekt, inspireras av andra eller lära dig mer om fri programvara. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Kaffe finns, och mitt på dagen brukar många gå ut och luncha tillsammans.

Vi öppnar dörrarna klockan 10 och håller öppet åtminstone till klockan 15. 

**Plats**: [Skolgatan 6](https://www.openstreetmap.org/node/9747185809)

