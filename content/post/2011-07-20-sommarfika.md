---
title: "Sommarfika"
date: 2011-07-20
---

Nu är det dags igen! På tisdag nästa vecka, den 2 augusti, träffas vi på Café Linné Konstantina för fika enligt konstens alla regler. Vädret får avgöra om vi sitter på uteserveringen eller en trappa upp inomhus.

För er som inte betalat medlemsavgiften än och hellre betalar kontant än via banköverföring går det bra att betala då.

Som vanligt är alla välkomna, medlemmar som icke medlemmar!