---
title: "Träff och årsmöte hos Freespee den 25:e maj!"
date: 2019-04-28
draft: false
---

Den 25:e maj så är det dags för hack och årsmöte igen. Denna gång kommer vi att vara hos Freespee som håller till på Dragarbrunnsgatan 78c. Vi öppnar dörrarna klockan 10, för att sedan röra oss hem vid 17.

Alla medlemmar är välkomna att vara med på årsmötet, men det är frivilligt. Föredrar du att hacka hela dagen så går det bra också.

Läs mer här - [meetup.com](https://www.meetup.com/Uppsala-Linux-User-Group/events/260781196/)
