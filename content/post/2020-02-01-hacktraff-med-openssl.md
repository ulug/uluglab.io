---
title: "Hackträff den 29:e februari!"
date: 2020-02-26
draft: false
---
# Hackträff på Folkuniversitetet

På lördag den 29 februari är det dags för ännu ett Linux-hack. 

Våra hackträffar brukar gå till så att vi öppnar klockan nio och stänger klockan sju, folk kommer och går som de vill, var och en sitter och jobbar med sitt om det inte är något utlyst tema. Vi brukar också presentera för varandra vad vi jobbar på under dagen. Utöver det är det ju mycket snack, tips och inspiration och goda möten. Vid tolvtiden brukar många gå ut och luncha tillsammans.

Det finns också möjlighet för dig som är nybörjare på Linux att få hjälp och tips, och hjälp att komma igång för dig som är nyfiken.

Det planerade besöket av en utvecklare från OpenSSL har tyvärr blivit inställt på grund av föredragshållarens återbud. Vi hoppas att han kan komma till oss i höst istället.

Platsen för träffen är [Studiefrämjandets lokaler på Ljusbärargatan 2 (f.d. Portalgatan 2B)](https://www.openstreetmap.org/way/98950286). Vi håller öppet mellan 9 och 19.
