---
title: "Kallelse till årsmöte, lör 6 maj"
date: 2023-03-11
draft: false
---

**Tid:** lördag den 6 maj klockan 14.

**Plats:** Studiefrämjandets lokaler på [Ljusbärargatan 2 (f.d. Portalgatan 2b)](https://www.openstreetmap.org/node/1144721787)

**Anmälan:** anmäl dig gärna genom att skicka ett mail till kontakt(snabel-a)ulug.se. Det är inte obligatoriskt att anmäla sig, men det hjälper oss att förbereda mötet om vi vet hur många som deltar.

Du kan också maila till kontakt(snabel-a)ulug.se om du vill lämna in en motion eller nominera dig själv eller någon till en förtroendepost (nomineringar kan även göras på mötet).

## Förslag till dagordning:

1. Mötets öppnande.
2. Fråga om mötets behöriga utlysande.
3. Val av ordförande för mötet, mötessekreterare, samt två justerare tillika rösträknare.
4. Fastställande av röstlängd.
5. Fastställande av dagordning.
6. Styrelsens verksamhetsberättelse.
7. Kassa och revisionsberättelse.
8. Frågan om avgående styrelses ansvarsfrihet.
9. Behandling av motioner och propositioner.
10. Fastställande av budget och medlemsavgift för det kommande året.
11. Fastställande av verksamhetsplan för det kommande året.
13. Val av ordförande och övriga ledamöter för det kommande året.
14. Fastställande av teckningsrätt för det kommande året.
15. Val av minst en revisor för det kommande året.
16. Övriga frågor.
17. Mötets avslutande.

## Medlemsavgift

Kom också ihåg att betala medlemsavgiften för 2023! Betald medlemsavgift ger rösträtt på årsmötet. Betala in 100 kr (eller valfritt belopp) till bankkonto 9022.69.789.84. Ange namn, alias eller e-postadress som referens vid inbetalningen. Läs mer på https://www.ulug.se/page/medlem/.

