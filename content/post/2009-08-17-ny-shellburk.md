---
title: "Shell.ulug.org - Nya skalburken!"
date: 2009-08-17
---

Hejsan alla ulugare!

Som ni säkert vet så har vi äntligen fått igång våran shellburk.
För att man ska få access till den, så har årsmötet bestämt att en mindre avgift kommer tas ut på 100 kr/år. Denna summa är till för att täcka diverse serverkostnader.