---
title: "Workshoplördag: Fediverse och säkerhetskopiering"
date: 2022-11-30
draft: false
---

Dags för Linux-hack igen. Den här gången djupdyker vi i två ämnen och håller workshops, en på förmiddagen och en på eftermiddagen. (Vill man komma och hänga utan att delta i workshopparna så går det såklart bra.)

## Mastodon och Fediverse

På sistone har *Mastodon* och *the Fediverse* varit som det surrats om. När Twitter nyligen bytte ägare lämnade många plattformen och sökte efter ett alternativ, och i och med detta har det decentraliserade nätverket Mastodon vuxit dramatiskt. Hur använder man det? Och vad menas med the Fediverse?

Under denna workshop lär vi oss att gå med i och använda Mastodon och tittar på hur det fungerar. Vi pratar om hur man väljer server och hur man följer folk på andra servrar. Vi tittar också på några av Mastodons "släktingar" till exempel Pixelfed och Peertube. Målgruppen är helt vanliga användare.

Del två riktar sig till den lite mer tekniske. Vi provar att sätta upp en egen mastodon-server, och kanske vi provar på att kopppla en blogg till Mastodon-nätverket.

Tid: Lördag 10 december, 10 - 12
Workshopledare: Jonarvid

## Säkerhetskopiering - strategier och teknik

Säkerhetskopiering kanske inte är det mest spännande inom IT, men betydligt roligare än om olyckan är framme och man råkar ut för en dataförlust. Din data kan vara allt ifrån dina kattbilder till kritisk företagsinformation, Det finns många kattbilder på internet men det är kanske inga som är som just dina, och troligen inte av dina katter. Och förlorad företagsdata kan vara slutet för en del företag. I denna workshop lär vi oss hur vi kan ta kontrollen över vår egen data.

Vi går igenom vilka strategier för säkerhetskopiering vi kan använda beroende på vilka risker vi ser. Vi tittar också närmare på verktyget BorgBackup, som är en populär fri programvara för säkerhetskopiering som kan köras direkt i terminalen. 

Tid: Lördag den 10 december, 14 - 16

- Plats: [Skolgatan 6 Coworking space](https://www.openstreetmap.org/node/9747185809)
- Tid: Vi öppnar dörrarna klockan 10 och håller öppet till klockan 16.

![Penguins by David Dibert](../../img/penguins-CC-David-Dibert.jpg "Penguins by David Dibert")
