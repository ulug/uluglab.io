---
title: "Hackträff lördag 7 december"
date: 2024-11-28
draft: false
---

Välkommen på hackträff **lördag den 7 december** i samarbete med Studiefrämjandet. 

![Studiefrämjandet](/img/studieframjandet-logo.png)

Denna gång har du chansen att lära dig grunderna i [LaTeX](https://en.wikipedia.org/wiki/LaTeX). LaTeX är ett typografisk programmeringsspråk som används för att skapa dokument eller presentationer, vetenskapliga artiklar, böcker och rapporter med hjälp av en mängd olika kommandon och syntax.

I övrigt välkommnar vi alla som vill komma och hänga med oss, snacka teknik, filosofi eller spännande hemmasnickrade projekt. Här finns utrymme för att lära av och inspireras av andra som också är intresserade av Linux och fri programvara.

För den som är nybörjare inom fri programvara kommer det att finnas stöd och hjälp att komma igång med Linux och tillhörande verktyg och program.

Programmet ser ut som följer:
- 10:00: Dörrarna öppnar
- 10:30: LaTeX for beginners (workshop held in english)
- 13:00: Gemensam lunch (för dem som vill)
- 13:00: Hack och lek
- 15:00: Avrundar för dagen

Kaffe kommer att finnas på plats.

**Plats**: [Ljusbärargatan 2b](https://www.openstreetmap.org/way/98950286)
