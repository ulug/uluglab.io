---
title: "Linuxfika onsdag 11 januari"
date: 2023-01-06
draft: false
---
Den 11 januari är inte bara årets andra onsdag, utan även den andra onsdagen i månaden. Och den andra onsdagen i månaden är det alltid Linux-fika!

Så välkommen onsdag den 11 januari till Café Linné, [Svartbäcksgatan 24](https://www.openstreetmap.org/node/459265410). Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux").
