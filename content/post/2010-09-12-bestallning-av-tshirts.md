---
title: "Beställning av Tshirts"
date: 2010-09-12
---

Dessa är ej tillgängliga at beställa längre.

Den 1 oktober kommer vi beställa ett gäng T-shirts. Priset blir 279 kronor styck (med nick +30 kronor). För beställning följ instruktionerna på T-shirt sidan. Observera att beställning och betalning måste vara klart före 1 oktober för att få det här priset.