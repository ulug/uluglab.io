---
title: "Online-event idag!"
date: 2020-10-17
draft: true
---

Idag har vi möte på Studiefrämjandet, men vi kör det även online. Vi har två möten: 

* själva hackträffen: https://public-meet2.glesys.com/ulug (redan igång)
* årsmötet: https://bigbluebutton.kodskolanknivsta.se/ (börjar klockan 14.00, insläpp från 13.45)

Vi ses!
