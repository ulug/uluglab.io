---
title: "Höstens första träff"
date: 2019-09-06
draft: false
---

Nu är sommarledigheten över, så nu är det dags att hacka igen. Den 21:a septermber så är träffas vi i Studiefrämjandets lokaler.

Läs mer här - [meetup.com](https://www.meetup.com/Uppsala-Linux-User-Group/events/264610915/)
