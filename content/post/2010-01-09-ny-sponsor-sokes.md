---
title: "Ny sponsor sökes"
date: 2010-01-09
---

Vår sponsor MrFriday AB kommer inte längre kunna erbjuda oss hosting av vår server. Därför söker vi nu efter en ny sponsor som kan erbjuda oss 4U rackutrymme, ett eluttag, internetanslutning och ett litet ( /29, gärna /28 ) nät med IP-adresser. Trafikmängden som används idag är väldigt liten.

Det ni får ut av det hela är er logo på vår website tillsammans med en länk till er website, men framförallt en massa goodwill hos Uppsalas Linuxanvändare.

Om ni har några möjligheter att erbjuda oss detta, kontakta styrelsen så snart som möjligt.