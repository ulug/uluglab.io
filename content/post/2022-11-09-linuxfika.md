---
title: "Linuxfika onsdag 9 november"
date: 2022-10-12
draft: false
---
Välkommen på Linux-fika på onsdag den 9 november på Café Linné, [Svartbäcksgatan 24](https://www.openstreetmap.org/node/459265410). Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux.

Vi har Linux-fika *andra onsdagen i månaden* och om inget annat sägs, så ses vi på Cafe Linné.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux").
