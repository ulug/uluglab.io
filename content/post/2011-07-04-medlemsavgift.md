---
title: "Medlemsavgift för verksamhetsåret 2011/2012"
date: 2011-07-04
---

Nu är det dags att betala medlemsavgiften för verksamhetsåret 2011 / 2012.

Årsmötet 2011 beslutade att införa en rekommenderad medlemsavgift på 50 kr. Det står dock alla medlemmar fritt att betala in ett större eller mindre belopp. De medlemmar som betalar in den rekommenderade medlemsavgiften eller mer får även ett shellkonto på föreningens server (observera att vi av säkerhetsskäl endast kan ge det till medlemmar som deltagit i våra fysiska aktiviteter som t.ex. fikaträffar, samt uppgett telefonnummer vid registreringen).

För att bli medlem eller förnya ditt medlemskap, ~~börja med att Registrera Dig eller Logga in!~~ Sedan kan du betala ditt medlemskap.

Betalning kan kan göras via överföring till föreningens bankkonto, eller kontant till kassör eller ordförande.