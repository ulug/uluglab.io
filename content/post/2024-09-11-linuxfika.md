---
title: "Linuxfika onsdag 11 september - ikväll på Årummet"
date: 2024-09-11
draft: false
---
I kväll ses vi på [Årummet](https://www.openstreetmap.org/node/4339484416) för Linuxfika (och inte på Café Linné som vi har brukat). Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux.

Årummet ligger i korsningen Östra Ågatan och St. Olovsgatan.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux")
