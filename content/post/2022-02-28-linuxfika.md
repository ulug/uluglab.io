---
title: "Linuxfika onsdag 2 mars kl. 19"
date: 2022-02-28
draft: false
---
Välkomna på Linux-fika på Café Linné, [Svartbäcksgatan 24](https://www.openstreetmap.org/node/459265410). Samtidigt kommer styrelsen ha öppet styrelsemöte/arbetsmöte. Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du är ny, så håll utkik efter ett bord med en liten Tux.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux").
