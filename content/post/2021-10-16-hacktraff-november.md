---
title: "Hackträff 20 november - Linux för nybörjare"
date: 2021-10-16
draft: false
---
Nu är det äntligen dags för hackträff igen på vårt gamla vanliga ställe, det vill säga, i [Studiefrämjandes lokaler](https://www.openstreetmap.org/way/98950286) på [Ljusbärargatan 2 (f.d. Portalgatan 2B)](https://www.openstreetmap.org/#map=16/59.8675/17.6371&layers=HN).

Denna gång fokuserar vi på nybörjarhjälp. Där alltså extra speciellt välkommen om du:

* är nyfiken på GNU/Linux och vill veta mer.
* vill ha hjälp att installera Linux för första gången.
* har problem med din Linuxdator och vill ha hjälp att lösa det

I övrigt utöver det kör vi som vanligt, det vill säga, folk kommer och går som de vill, var och en jobbar med sitt. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Utöver det finns det tid för snack, tips och inspiration och goda möten. Mitt på daten brukar många gå ut och luncha tillsammans.

Vi öppnar dörrarna klockan 10 och håller öppet till klockan 19. 
