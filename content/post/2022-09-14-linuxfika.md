---
title: "Linuxfika onsdag 14 september"
date: 2022-09-12
draft: false
---
Vi startar upp våra regelbundna Linux-fikan igen, och siktar på att ses *andra onsdagen i månaden* på Café Linné, [Svartbäcksgatan 24](https://www.openstreetmap.org/node/459265410). Vi träffas klockan 19 och sitter kvar tills vi lessnar. Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux.

![En 3D-printad Tux-pingvin](/img/tux.jpg "Håll utkik efter ett bort med Tux").
