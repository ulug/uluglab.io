---
title: "Hackträff 23 mars"
date: 2024-03-20
draft: false
---

Äntligen har vi en hackträff planerad. Det blir **lördag den 23 mars** i samarbete med Studiefrämjandet.

![Studiefrämjandet](/img/studieframjandet-logo.png)

Det blir en workshop där vi hjälps åt att bygga din egen webbaserade mediaserver för film, TV, ljudböcker, musik, tecknade serier, med mera med hjälp av Jellyfin (mediaserver), Sonarr (hanterar TV-serier), Radarr (film), Lidarr (musik), Bazarr (undertexter), samt Jackett (torrent-sökning) och Deluge (nedladdning).

Vi kommer att bygga detta inuti en LXC container på en Ubuntu server med hjälp av Ansible (Ansible skript kommer att tillhandahållas via öppna git-förråd).

Din mediaserver låter dig själv (eller de du bjuder in) styra över och titta på innehållet och laddar automatiskt ner saker som du väljer att bevaka.

The workshop can be conducted in English if the participants request it.



I övrigt är du välkommen att komma och hänga med oss, snacka teknik eller filosofi, jobba på dina eventuella projekt, inspireras av andra eller lära dig mer om fri programvara. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Kaffe finns, och mitt på dagen brukar många gå ut och luncha tillsammans.

Är du nybörjare eller bara nyfiken på fri programvara, så är du särskilt välkommen. Om du har problem med din linuxdator, så är det ett utmärkt tillfälle att få hjälp.

Vi öppnar dörrarna klockan 10, föredraget börjar kl 11 och vi håller öppet till klockan 15. 

**Plats**: [Ljusbärargatan 2](https://www.openstreetmap.org/way/98950286)


Vi vill också passa på att tipsa om två kvällsaktiviteter - eller After Hack, om man så vill:

1. På dataföreningen Update kan man höra ett föredrag om programmeringsspråket Forth, ett klurigt språk som inte liknar något annat. Kl 19 på Svartbäcksgatan 65. https://wiki.dfupdate.se/projekt:updateringar

2. Är man upplagd för "En kväll med ljudlandskap, ambientmusik och experimentell dans" ska man istället ta sig till Sprakateatern på Salagatan 24 klockan 19. 
