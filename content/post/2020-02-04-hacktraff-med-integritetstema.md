---
title: "CANCELLED - Mitigating Mass Surveilance - tools and techniques, 22 mars"
date: 2020-03-10
draft: false
---

## This event has been cancelled - denna tillställning är inställd
*Vi hoppas att vi kan anordna detta när läget är klarare*

Lär dig hur du skyddar ditt privatliv på nätet! I november hade ULUG besök av Linus Nordberg från Föreningen för digitala fri och rättigheter, [DFRI](https://www.dfri.se/). Då talade han på temat _Problemen med massövervakning_. 

Lördag den 21 mars kommer han tillbaks till oss, nu med mer handfasta tips, det vill säga metoder och verktyg för att trygga privatlivet.

Vi startar sammankomsten med Linus' föredrag. På eftermiddagen håller vi workshops där vi lär oss använda vissa av de verktyg som vi talar om.

Alla är välkomna, både nybörjare och erfarna användare. Vi anpassar workshoparna till deltagarnas nivå. Glöm inte att ta med din laptop!
  
### Program:
11:00. Föredrag: Mitigating Mass Surveillance (english)
12:00. Lunch (rummet är öppet för dem som vill stanna kvar och hakka)
13:30. Workshopparna börjar
15:30. Slut.

### Plats
Base10, [Stationsgatan 25](https://www.openstreetmap.org/#map=18/59.85621/17.65251), Uppsala

Vi tackar [Base10](https://base10.com) för att vi får nyttja deras lokal. Gå gärna in på deras [eventsida](https://base10.com/events) för att se vad mer som är på gång!

![Base10 logo](/img/base10logo.png)

Om du vill får du anmäla dig på [https://www.meetup.com/Uppsala-Linux-User-Group/events/268461483/](meetup.com).

## Mitigating Mass Surveillance -- Tools and Techniques 

Learn how to protect your privacy online! Once again, we invite Linus Nordberg from [DFRI](https://dfri.se), an organisation for digital rights, to speak about the problems with mass surveillance.

This time, we will focus on tools and techniques that we can use to protect our privacy online. We open the event with Linus' talk. After lunch, we have workshops on some of the tools that Linus talks about, depending on what we're most interested in. 

Both beginners and experienced users are welcome! We'll adapt the workshops to your level. Don't forget to bring your laptop!

### Schedule
11:00. Talk: Mitigating Mass Surveillance
12:00. Lunch break (room will be open for those who want to hack on their computer)
13:30. Workshops
15:30. End

### Location
Base10, [Stationsgatan 25](https://www.openstreetmap.org/#map=18/59.85621/17.65251), Uppsala

Thanks to [Base10](https://base10.com) that we can use their meeting room. Please visit their [event page](https://base10.com/events)!

![Base10 logo](/img/base10logo.png)

Optional registration: [https://www.meetup.com/Uppsala-Linux-User-Group/events/268461483/](meetup.com).
