---
title: "Linuxhäng på videolänk"
date: 2021-04-25
draft: false
---

Vårt förra videomöte i mars blev populärt och nu kör vi en uppföljare. Vi startar klockan 18 och håller på så länge som folk vill hänga kvar. Om du har ett förslag på tema för träffen, så hör av dig till oss på kontakt[snabel-a]ulug.se.

## ULUG bjuder in till videohäng! 

* Tid: lördag den 8 maj kl. 18.00
* Plats: (https://public-meet2.glesys.com/ulug-8-maj) - Vi använder videomötesplattformen Jitsi, och publicerar en länk när mötet börjar närma sig.

Välkommen! 

