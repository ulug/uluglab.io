---
title: "Opensource på Stadsbiblioteket"
date: 2019-10-30
draft: false
---

Den 23:e nonvember så kör vi ett event på Stadsbiblioteket. Vi kommer att ha talare från DFRI och Mycroft. Varmt välkomna klockan 12:00.

Eventet utförs i samarbete med Studiefrämjandet.

Läs mer här - [meetup.com](https://www.meetup.com/Uppsala-Linux-User-Group/events/265402199/)
