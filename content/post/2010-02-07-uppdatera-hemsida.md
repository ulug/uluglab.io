---
title: "Uppdaterad hemsida"
date: 2010-02-07
---

Nu har vi uppdaterat hemsidan till version 1.0.1 av FöreningsCMS. I samband med detta har vi lagt till en kalender där alla fika och andra händelser i föreningens regi dyker upp. Kalendern hittar du i menyn till vänster. En annan nyhet är en sida med information om den sittande styrelsen.