---
title: "Träff Samt Årsmote"
date: 2018-05-14
draft: false
---

26:e maj är det åter dags för en heldag med ULUG mini-hackathon!

Dagen bjuder även på årsmöte som är frivilligt.

Schema:

* Vi börjar kl 09.30 och håller på fram till kl 20.00.
* 11.00 Årsmöte (se tidigare utskick - man behöver INTE gå på detta bara för att man är på träffen, mötet sker i separat rum)

I övrigt gäller det vanliga - hacka på ditt projekt, hårdvara som mjukvara. Installera en exotisk Linuxvariant, eller bjud med någon som aldrig provat fri mjukvara - Valet är ditt! Detta är ett tillfälle att diskutera teknik och lösningar med den stora samlade kunskapsbasen som finns i ULUG, och ett tillfälle att utbyta erfarenheter med andra medlemmar.

Oavsett om du vill sitta och hacka i gott sällskap, eller jobba i något gemensamt projekt är du varmt välkommen till ULUG mini-hackathon. Vill du bara dricka kaffe och prata med folk utan att ha något direkt projekt går det lika bra!

Du sätter själv ambitionsnivån på vad du vill göra med denna dag.

Anmäl dig på
https://www.meetup.com/Uppsala-Linux-User-Group/events/250033688/

Se också https://www.facebook.com/ulug.org

ULUG mini-hackathon arrangeras i samarbete med Studiefrämjandet.