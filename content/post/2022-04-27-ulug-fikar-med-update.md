---
title: "Gemensam träff med datorföreningen Update, onsdag 4 maj kl 18"
date: 2022-04-30
draft: false
---
## Uppdaterat Linux-fika

Sedan en tid tillbaka har vi Linuxfika första onsdagen varje månad. Kommande onsdag har vi blivit inbjudna av våra vänner i datorföreningen Update - som också har träffar på onsdagar - till en gemensam träff.

- Tid: onsdag den 4 maj kl 18
- Plats: [Indian Mahal](https://www.openstreetmap.org/node/4362453170), Svartbäcksgatan 73, därefter i [Updates föreningslokal](https://www.openstreetmap.org/node/9418755099) på Svartbäcksgatan 65.

Update skriver:

> Vi vill lära känna er för att se om det finns möjlighet till samarbete mellan föreningarna och i det fallet spåna på hur något sådant skulle kunna se ut! Kl. 18:00 bjuder vi in er att äta med oss på Indian Mahal på Svartbäcksgatan 73. Efteråt kan vi gå vidare till Updates föreningslokal på Svartbäcksgatan 65 och ge er en rundtur.

> Vi ser fram emot att träffa er,
