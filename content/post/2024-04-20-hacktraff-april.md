---
title: "Hackträff 20 april"
date: 2024-03-23
draft: false
---

Välkommen till ännu en hackträff, lördag den 20 april i samarbete med Studiefrämjandet.

![Studiefrämjandet](/img/studieframjandet-logo.png)

Vi får då besök av Samuel från [Kamratdataföreningen Konstellationen](https://konstellationen.org/om-oss/) som jobbar med att "progressiva sociala rörelser att ha egna kommunikationskanaler och att det behövs fler plattformar, verktyg och sociala rum på nätet där det är användarna som har makten och inte företagen."

I övrigt är du välkommen att komma och hänga med oss, snacka teknik eller filosofi, jobba på dina eventuella projekt, inspireras av andra eller lära dig mer om fri programvara. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Kaffe finns, och mitt på dagen brukar många gå ut och luncha tillsammans.

Är du nybörjare eller bara nyfiken på fri programvara, så är du särskilt välkommen. Om du har problem med din linuxdator, så är det ett utmärkt tillfälle att få hjälp.

Vi öppnar dörrarna klockan 10, föredraget börjar kl 11 och vi håller öppet till klockan 15. 

**Plats**: [Ljusbärargatan 2](https://www.openstreetmap.org/way/98950286)
