---
title: "Årsmötesprotokoll 2010"
date: 2010-07-01
---

Bifogat finns protokollet (samt signaturfiler) från årsmötet 2010-06-01.

[Protokoll](/protokoll/2010/2010-06-01 Årsmötesprotokoll.pdf)  
[Signatur Anders](/protokoll/2010/2010-06-01 Årsmötesprotokoll.pdf.sig-anders)  
[Signatur Martin](/protokoll/2010/2010-06-01 Årsmötesprotokoll.pdf.sig-martin)  
[Signatur Mats](/protokoll/2010/2010-06-01 Årsmötesprotokoll.pdf.sig-mats)  
[Signatur Mats-Erik](/protokoll/2010/2010-06-01 Årsmötesprotokoll.pdf.sig-mats-erik)