---
title: "Linux 30 år-picknick"
date: 2021-08-30
draft: false
---

![Celebrate 30 years of Linux](https://linuxfoundation.org/wp-content/uploads/tuxturns30_soc1.jpg)

Det var längesen vi hade möjlighet att ha en fysisk träff, men vi tänkte göra ett försiktigt försök, nu när många har fått vaccin, och vi fortfarande kan vara utomhus. Linux har i dagarna fyllt **30 år** och det firar vi med att ha picknick i parken!

- Plats: [Stadsträdgården](https://www.openstreetmap.org/#map=18/59.85359/17.64183)
- Dag: lördag den 4 september
- Tid: 13.00 - 15.00

Ta med:
- en filt eller stol, 
- mat och dryck om du vill, 
- en vän
- laptop med välladdat batteri
- ett glatt kalashumör :-)

Några förhållningsregler:

- Stanna hemma om du har symptom
- Håll avstånd
- Inga blöta pussar
- Använd lösenordshanterare och tvåfaktorsautentisering

(Om det blir regn, blir det inställt)
