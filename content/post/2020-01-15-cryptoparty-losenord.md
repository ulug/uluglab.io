---
title: "Cryptoparty på lösenordsbytardagen"
date: 2020-01-15
draft: false
---

Vi vill tipsa om att [Cryptoparty Uppsala](http://user.it.uu.se/~arvge836/cryptoparty/) har en tillställning på måndag den 20 januari med anledning av [Lösenordsbytardagen](https://pcforalla.idg.se/om/l%C3%B6senordsbytardagen). Det blir föredrag om hur man skapar säkra lösenord och om hur man använder en lösenordshanterare. Det blir också diskussioner och fika. Samlingen hålls på Carolina Redivivas kafé mellan klockan 17 och 19.

Läs mer här - [Cryptoparty Uppsala](http://user.it.uu.se/~arvge836/cryptoparty/#upcoming).
