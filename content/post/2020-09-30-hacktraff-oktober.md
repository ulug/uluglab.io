---
title: "Hackträff lördag den 17 oktober!"
date: 2020-09-29
draft: false
---
På lördag den 17 oktober är det dags för ännu ett Linux-hack, efter lång väntan.

Våra hackträffar brukar gå till så att vi håller öppet några timmar under dagen då folk kommer och går som de vill, var och en sitter och jobbar med sitt om det inte är något utlyst tema. Vi brukar också presentera för varandra vad vi sysslar med under dagen. Utöver det finns det tid för snack, tips och inspiration och goda möten. Vid tolvtiden brukar många gå ut och luncha tillsammans.

Det finns också möjlighet för dig som är nybörjare på Linux att få hjälp och tips, och hjälp att komma igång för dig som är nyfiken.

På grund av risken för smittspridning är antalet platser begränsat till 16. Det är också viktigt att vi alla tänker på att

* stanna hemma vid symptom
* håll avstånd
* tvätta händerna

Under dagen kommer också årsmötet att hållas, vilket hålls via videomöte, se [kallelsen till årsmöte](https://ulug.org/post/2020-09-16-kallelse-till-arsmote/). 

Platsen för träffen är [Studiefrämjandets lokaler på Ljusbärargatan 2 (f.d. Portalgatan 2B)](https://www.openstreetmap.org/way/98950286). Vi håller öppet mellan 10 och 16.
