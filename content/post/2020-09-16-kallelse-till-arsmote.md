---
title: "Kallelse till årsmöte"
date: 2020-09-16
draft: false
---

**Tid:** lördag den 17 oktober klockan 14.

**Plats:** årsmötet kommer att hållas via videokonferens. Länk till mötet: https://bigbluebutton.kodskolanknivsta.se/

**Anmälan:** anmäl dig gärna genom att skicka ett mail till kontakt(snabel-a)ulug.se. Det är inte obligatoriskt att anmäla sig, men det hjälper oss att förbereda mötet om vi vet hur många som deltar, och det gör också att vi kan kontakta dig om "tekniken strular" vid mötet.

Om du vill nominera någon till en förtroendepost kan du också maila till kontakt(snabel-a)ulug.se (nomineringar kan även göras på mötet).

## Förslag till dagordning:

1. Mötets öppnande.
2. Fråga om mötets behöriga utlysande.
3. Val av ordförande för mötet, mötessekreterare, samt två justerare tillika rösträknare.
4. Fastställande av röstlängd.
5. Fastställande av dagordning.
6. Styrelsens verksamhetsberättelse.
7. Kassa och revisionsberättelse.
8. Frågan om avgående styrelses ansvarsfrihet.
9. Behandling av motioner och propositioner.
10. Fastställande av budget och medlemsavgift för det kommande året.
11. Fastställande av verksamhetsplan för det kommande året.
13. Val av ordförande och övriga ledamöter för det kommande året.
14. Fastställande av teckningsrätt för det kommande året.
15. Val av minst en revisor för det kommande året.
16. Övriga frågor.
17. Mötets avslutande.

## Medlemsavgift

Kom också ihåg att betala medlemsavgiften för 2020! Betald medlemsavgift ger rösträtt på årsmötet. Betala in 100 kr (eller valfritt belopp) till bankkonto 9022.69.789.84. Ange din e-postadress som referens vid inbetalningen. Läs mer på https://www.ulug.se/page/medlem/.

