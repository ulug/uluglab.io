---
title: "Hackträff 9 november"
date: 2024-10-28
draft: false
---

Välkommen på hackträff **lördag den 9 november** i samarbete med Studiefrämjandet. 

![Studiefrämjandet](/img/studieframjandet-logo.png)

Denna gång har vi inget särskilt tema men vi mottar gärna förslag. Har du något du gärna pratar om finns det utrymme att hålla föredrag.

För den som är nybörjare inom fri programvara kommer det att finnas stöd och hjälp att komma igång med Linux och tillhörande verktyg och program.

I övrigt välkommnar vi alla som vill komma och hänga med oss, snacka teknik, filosofi eller spännande hemmasnickrade projekt. Här finns utrymme för att lära av och inspireras av andra som också är intresserade av Linux och fri programvara.

Dagen brukar se ut som följer:
- 10:00: Dörrarna öppnar
- 10:30: Kort presentationsrunda
- 10:45: Hack och lek
- 12:00: Gemensam lunch
- 13:00: Hack och lek
- 15:00: Avrundar för dagen

Kaffe kommer att finnas på plats.

**Plats**: [Ljusbärargatan 2b](https://www.openstreetmap.org/way/98950286)
