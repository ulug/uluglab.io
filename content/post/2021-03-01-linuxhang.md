---
title: "Linuxhäng på videolänk"
date: 2021-03-01
draft: false
---

Äntligen händer det! Nästan ett år efter alla andra i samhället kom vi i ULUG-styrelsen på att vi kunde använda videoströmmar via tcp/ip för att socialisera. Därför presenterar vi nu stolt: 

## ULUG bjuder in till videohäng! 

* Tid: Torsdag den 4 mars klockan 19
* Plats: https://public-meet2.glesys.com/ulug

Välkommen! (och tycker du det var lite snabbt inpå och du inte har möjlighet att vara med, så håll utkik: det blir nog fler tillfällen!)

