---
title: "Inbjudan från datorföreningen Update"
date: 2022-04-08
draft: false
---

Hi all,

you're invited to the [Update computer club](https://www.dfupdate.se/en/) public lecture series [Updateringar](https://wiki.dfupdate.se/projekt:updateringar)!

- When:  2022-04-09, 19:00 CEST
- Where: https://bbb.cryptoparty.se/b/upd-0mo-m2u-aq8

## A tour of Update's new premises

In December and January 2021/2022 Update moved out of the Uppsala University IT department's basement into rooms of its own (thanks again to all our volumteers who put in a huge effort!). The new premises offer 150 m² of space for Update's collection and activities, with a dedicated area for exhibitions.

What has happened since the move? With this online tour we invite you to take a peek into our new home and at what we've been working on in the last couple of months. We will give you an insight into current and future projects and show off some of our collection items.

Bjarni Juliusson, Anke Stüber (Update)

The lecture is free and open to everyone.

Don't want to miss upcoming events? Subscribe to our [low-traffic
announcement list here](https://lists.dfupdate.se/postorius/lists/announce.lists.dfupdate.se)

Hope to see you there,
Anke/zeltophil
