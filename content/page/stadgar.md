---
title: Stadgar
subtitle: Uppsala LINUX User Group (ULUG)
comments: false
---

#### §1 Föreningens namn, form, syfte och varaktighet

* Föreningens namn är Uppsala LINUX User Group, förkortat ULUG.

* Föreningen har sitt säte i Uppsala. Föreningen är en ideell förening.

* Föreningen är politiskt och religiöst obunden, samt öppen för alla.

* Föreningen ULUG verkar i första hand i Uppsala med omnejd och har som huvudsakligt syfte:

    * _att främja kunskaper och erfarenhetsutbyte om i synnerhet GNU och LINUX, men även annan fri eller öppen programvara_  
    * _att informera om i synnerhet GNU och LINUX, men även annan fri eller öppen programvara_  
    * _att främja användandet av i synnerhet GNU och LINUX, men även annan fri eller öppen programvara._  
    * _Också relaterad programvara omfattas av verksamhetsområdet._  

* Föreningen kan även syssla med övriga aktiviteter som på något sätt kan vara relaterade till ovanstående.

* Upplösning av föreningen:
 För upplösning av föreningen krävs att medlemsantalet understiger 10 st., och beslut med 2/3 delars majoritet av två på varandra följande möten av typen års- eller föreningsmöte, med minst två månaders mellanrum, varav ett skall vara ordinarie årsmöte.

* Beslut om upplösning av föreningen kan endast fattas av årsmöte, på vars dagordning ärendet varit uppfört i samband med kallelsen. Årsmötet beslutar i händelse av upplösning hur det skall förfaras med föreningens eventuella tillgångar/skulder.

#### §2. Medlemskap

* Föreningsmedlem godkänner föreningens stadgar och att föreningen lagrar och hanterar vederbörandes personuppgifter, även på datamedia.

* Inträde:
Som medlem räknas den som erlagt medlemsavgift för innevarande verksamhetsår, samt uppgivit namn och e-postadress.

* Medlemsavgift:
Medlemsavgiftens storlek för kommande år bestäms på årsmötet. Erlagd medlemsavgift återbetalas ej.

* Utträde:  
   Medlemskap upphör i följande fall:

    * på egen skriftlig begäran och då med omedelbar verkan, undantaget styrelsemedlem, som måste anmäla utträde minst 2 månader i förväg.  
    * om medlemsavgift ej erläggs på anmodan inom angiven tid.  
    * om medlemmen utesluts av föreningsmöte, på grund av att denne orsakat föreningen skada eller motverkat dess syfte.  
    * Medlem som orsakat föreningen skada eller motverkar dess syfte kan under en angiven tidsperiod, dock längst fram till närmaste förenings- eller årsmöte, avstängas av styrelsen, som förutom medlemmen ifråga måste vara enig. Avstängningsfrågan skall tas upp och beslutas om av första förenings-/årsmöte efter avstängningen. Medlemmen skall då ges tillfälle att yttra sig. Beslut om förlängd tidsbestämd avstängning eller eventuell åtföljande definitiv uteslutning måste stödjas av minst 3⁄4 av föreningsmötet respektive årsmötet, undantaget den berörda medlemmen. Vid lika röstetal avgör lotten.

* Avstängning är verksam under av föreningsmötet fastställd tid. Om uteslutning kan endast beslutas av årsmöte.

#### §3. Förtroendevalda, styrelse

* Alla förtroendevalda utom revisorer måste vara medlemmar i föreningen.
Styrelsen kan ha 3 - 8 medlemmar inklusive ev. suppleanter, men skall minst bestå av:

    * ordförande
    * kassör
    * sekreterare

* Styrelsen väljs för ett verksamhetsår av årsmötet

* Styrelsen sammanträder minst två gånger per år på ordförandens kallelse, vilken skall vara styrelseledamöterna tillhanda senast en vecka före mötet. Kallelsen kan vara muntlig.

* Varje styrelsemedlem äger rätt att hos ordföranden begära sammankallande av styrelsemöte.

* Beslut fattas med enkel majoritet. Vid lika röstetal har ordföranden utslagsröst, utom i personfrågor, då lotten avgör.

* För att styrelsen skall äga beslutsrätt krävs att minst hälften av styrelsemedlemmarna är närvarande. Vid styrelsemöte skall skriftligt protokoll föras. Styrelsemötenas protokoll är ej i sin helhet offentliga. Styrelsen avgör vilka delar som skall vara tillgängliga för hela föreningen.

#### §4. Verksamhetsår, mötesordning

* Föreningens verksamhetsår är från den 1 juli till den 30 juni.

* Årsmöte och föreningsmöte
Årsmötet är föreningens högsta beslutande organ. Alla medlemmar har närvaro-, yttrande- och rösträtt. Årsmötet beslutar i följande frågor:

    * val av styrelse
    * ansvarsfrihet för avgående styrelse
    * stadgefrågor, i synnerhet om föreningens inriktning
    * avstängning och uteslutning (personfrågor)
    * Föreningsmöten beslutar i följande frågor:

        löpande verksamhetsfrågor som styrelsen eller 1/4 av medlemmarna funnit av sådan vikt att hela föreningen bör beredas möjlighet att ta ställning, yttra sig och rösta om.

    * avstängning (personfrågor)
    * Fullmakter godkänns ej.
    * Ordinarie årsmöte äger rum en gång om året på våren.

* Extra årsmöte kan sammankallas av styrelsen eller om minst 1⁄4 av medlemmarna så kräver, och har då samma beslutsrätt som ordinarie årsmöte.

* Kallelse till årsmöte skall ske skriftligen senast 4 veckor innan mötets datum. Dagordning skall bifogas kallelsen.

* Föreningsmöte kan sammankallas av styrelsen eller om minst 1⁄4 av medlemmarna så kräver, och har då beslutsrätt i de frågor som angivits i dagordningen som bifogats kallelsen, utom årsmötesfrågor. Kallelse till föreningsmöte skall ske skriftligen senast 1 vecka innan mötets datum. Dagordning skall bifogas kallelsen.

* Kallelse kan ske på elektronisk väg, utom till medlem som krävt att få denna per snigelpost, och då på egen bekostnad.

* På årsmötet utses styrelse, föreningsrevisor och bestäms vem som har teckningsrätten för det kommande året, samt fastställs budget och verksamhetsplan.

* På årsmötet måste antingen minst 5 medlemmar eller minst hälften av medlemmarna närvara för att det skall vara beslutsmässigt.

* På föreningsmöten inklusive årsmötet har alla medlemmarna varsin röst. Vid lika resultat har ordförande utslagsröst, utom i personfråga.
Omröstningar sker normalt medelst acklamation, om inte rösträkning begärts. Röstning sker då antingen medelst handuppräckning eller som sluten omröstning. Vid frågan om styrelsens ansvarsfrihet får inte styrelsen rösta. Vid lika röstetal i denna fråga så har föreningsrevisorn utslagsröst.

* Personfrågor skall avgöras i sluten omröstning med majoritetsbeslut. Vid lika röstetal avgör lotten.

* Inkomna motioner och propositioner skall bifogas kallelsen. De kända enskilda motionerna och propositionerna skall framgå av dagordningen. Motioner skall sändas in i elektronisk form. Mötet kan även tillåta motioner direkt på mötet under fastställandet av dagordningen.

* På ordinarie årsmöte, extraordinarie årsmöte och föreningsmöte skall minst följande punkter upptas på dagordningen:

    * Mötets öppnande
    * Fråga om mötets behöriga utlysande.
    * Val av ordförande för mötet, mötessekreterare, samt 2 justerare tillika rösträknare.
    * Fastställande av dagordningen.
    * Behandling av motioner och propositioner.
    * Övriga frågor.
    * Tid för nästa möte.
    * Mötets avslutande.
    * På ordinarie årsmöte skall dessutom följande punkter ingå:

    * Styrelsens verksamhetsberättelse
    * Kassa och revisionsberättelse
    * Frågan om avgående styrelses ansvarsfrihet
    * Val av ordförande, vice ordförande, kassör, sekreterare och ledamöter för det kommande året.
    * Fastställande av teckningsrätt för det kommande året.
    * Val av minst en revisor för det kommande året.
    * Ev. övriga val.
    * Fastställande av budget och medlemsavgift för det kommande året.
    * Fastställande av verksamhetsplan för det kommande året.

        Vid mötena skall skriftligt protokoll föras, vilka skall justeras. Föreningen skall efter möjlighet ordna sammankomster som gagnar föreningens syfte. Sammankomsterna kan vara öppna för endast medlemmar eller för alla intresserade.

#### §5. Revision

* Till grund för revision av verksamheten ligger stadgarna och svensk lagstiftning. Revision av föreningens verksamhet, bokföring och bokslut utföres av minst en revisor. Revisorn (revisorerna) utses av årsmötet. Revisorerna framlägger till årsmötet sin berättelse för föregående verksamhetsår tillsammans med till- eller avstyrkande av ansvarsfrihet för styrelsen.

#### §6. Stadgar

* För att dessa stadgar skall ändras, krävs 3/4 majoritet i röstetalet på 2 på varandra följande årsmöten med minst 4 veckor emellan. Ärendet skall ha framgått av möteskallelserna.
