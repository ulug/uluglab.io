---
title: Styrelse
comments: false
---

Föreningen leds av en demokratiskt vald styrelse bestående av 3 - 8 medlemmar. Styrelsen sammanträder vid behov, dock minst två gånger per verksamhetsår. Styrelsen nås via mail till [styrelse(snabel-a)ulug.se](mailto:styrelse(snabel-a)ulug.se).

Styrelsen har följande sammansättning:

- Ordförande: Jon Johnson
- Kassör: Jonatan Andersson
- Ledamöter:

  - Rikard Lindström
  - Mattias Nyberg

* Revisor: Karl Ljungkvist
