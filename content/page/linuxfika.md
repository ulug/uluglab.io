---
title: "Linuxfika"
subtitle: "Andra onsdagen varje månad"
comments: false
---
En av våra mest regelbundna aktiviter är vårt linuxfika. Då träffas vi och fikar och pratar, det kan vara om fri programvaru-filosofi och samhällsutveckling lika väl som om de senaste programmen eller hårdvaran, spel eller vad som helst. Det kan också vara ett bra tillfälle att få hjälp med något it-relaterat problem. Det varierar hur många som kommer men vi brukar bli mellan tre och tretton pers.

Vi träffas andra onsdagen varje månad klockan 19 på [Café Årummet](https://www.openstreetmap.org/node/4339484416) (om inget annat meddelas).

Om du träffar oss för första gången och undrar hur du hittar oss, så håll utkik efter ett bord med en liten Tux. ![En 3D-printad Tux-pingvin](/img/liten-tux.jpg "Håll utkik efter ett bort med Tux").

## Kommande fikaträffar

Datum för de närmaste fikaträffarna är:

- ons 8 januari
- ons 12 februari
- ons 12 mars
- ons 9 april
