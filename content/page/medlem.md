---
title: Medlemskap
subtitle: För linux i Uppsala sedan 2002
comments: false
---

Genom att bli medlem, stöder du föreningens syfte att främja kunskaper och erfarenhetsutbyte om GNU/Linux och annan fri och öppen mjukvara.

Den rekommenderade medlemsavgiften är 128 kr. Det står dock alla medlemmar fritt att betala in ett större eller mindre belopp, dock minst 1 kr. Betala in avgiften till **9670-2185008** (JAK Medlemsbank). Ange din e-postadress, alias eller någon annan unik information som gör att vi kan koppla ihop dig med betalningen. E-posta sedan denna information till kontakt(snabel-a)ulug.se. Pengarna går bland annat till lokalhyror, reseersättningar för föreläsare på våra träffar samt administration.

Medlemsavgiften för det kommande verksamhetsåret bör vara betald senast den sista juni. Nya medlemmar som betalar in medlemsavgiften under ett pågående verksamhetsår blir medlemmar från och med den dag betalningen kommit in till och med verksamhetsårets slut. Sedan förnyas medlemskapet enligt ovan.

Som ny medlem läggs du till på vår sändlista (e-postlista) med utskick om de aktiviteter vi anordnar. Vi lovar att inte spamma er. Är du inte med på listan, så kan du signa upp dig genom att skicka ett e-postmeddelande till utskick-request(snabel-a)ulug.se?subject=subscribe.

Du kan även delta i föreningens diskussionslista. Här kan du gå med om du vill diskutera med andra ulug-medlemmar, om ULUG:s verksamhet eller Linux- eller FOSS-relaterat i allmänhet. Signa upp genom att skicka ett mail till diskussion-request(snabel-a)ulug.se?subject=subscribe.

Utdrag från föreningens [stadgar](https://www.ulug.se/page/stadgar):

```
§2. Medlemskap
Föreningsmedlem godkänner föreningens stadgar och att föreningen lagrar och hanterar vederbörandes personuppgifter, även på datamedia.

Inträde:
Som medlem räknas den som erlagt medlemsavgift för innevarande verksamhetsår, samt uppgivit namn, adress och e-postadress.

Medlemsavgift:
Medlemsavgiftens storlek för kommande år bestäms på årsmötet. Erlagd medlemsavgift återbetalas ej.

```
