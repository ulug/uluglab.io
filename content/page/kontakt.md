---
title: Kontakt
subtitle: Att nå styrelse och andra medlemmar
comments: false
---

## E-post

Om du vill kontakta föreningen, kan du e-posta till kontakt(snabel-a)ulug.se. Vill du kontakta ordförande direkt, kan du skriva till ordforande(snabel-a)ulug.se.

## E-postlistor/sändlistor

När du blir medlem, lägger vi till dig på föreningens sändlista (e-postlista). Där får du inbjudningar till våra träffar och andra aktiviteter. Är du inte med på listan, men vill gå med, skickar du ett e-postmeddelande till [utskick-request(snabel-a)ulug.se](mailto:utskick-request(snabel-a)ulug.se?subject=subscribe). Vill du se tidigare utskick, titta i [arkivet](https://lists.fripost.org/ulug.se/sympa/arc/utskick/).

Du kan även delta i föreningens diskussionslista. Här kan du gå med om du vill diskutera med andra ulug-medlemmar, om ULUG:s verksamhet eller Linux- eller FOSS-relaterat i allmänhet. Signa upp genom att skicka ett mail till diskussion-request(snabel-a)ulug.se?subject=subscribe. Vill du se tidigare diskussioner, titta i [arkivet](https://lists.fripost.org/ulug.se/sympa/arc/diskussion/).

## Webb och RSS

Vår [webbplats](https://www.ulug.se) är vår primära informationskanal. Du kan också få våra nyheter som [RSS-flöde](https://www.ulug.se/index.xml).

## Mastodon och Matrix

Följ oss på Mastodon: https://social.linux.pizza/@ulug och diskutera med andra medlemmar via [Matrix](https://matrix.org/try-matrix/) #ulug:matrix.org
