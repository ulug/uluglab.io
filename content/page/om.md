---
title: Om ULUG
subtitle: Uppsala Linux User Group
comments: false
---

ULUG syfte är att främja kunskaper och erfarenhetsutbyte om i synnerhet GNU/Linux och annan fri/öppen mjukvara (som också går under beteckningen [FOSS](https://sv.wikipedia.org/wiki/FOSS)). Föreningen grundades tisdag 28 maj 2002 och är en ideell förening som är öppen för alla, politiskt och religiöst obunden.

Vi är i första hand en social förening som anordnar träffar där Linux-användare kan träffa likasinnade, med bland annat mini-hackathons och sporadiska föreläsningar.

Se även våra [stadgar](/Stadgar.pdf).
