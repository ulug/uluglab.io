To add a new post to the site use the below template.

File name should follow this convention 2000-01-01-name-of-post.md

Place the file in content/post

Template to use for the meta data:

---
title: "Träff hos Freespee 29:e september!"
date: 2018-08-22
draft: false
---
